MailFetch::Application.routes.draw do

  namespace :api, defaults: { format: :json } do
    get :contacts, controller: :contacts, action: :index
  end
end

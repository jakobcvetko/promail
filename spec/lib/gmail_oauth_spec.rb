require 'spec_helper'
require 'gmail_oauth'

describe GmailOauth do

  let(:email) { "jakob.boss@gmail.com" }
  let(:token) { "ya29.1.AADtN_W059qsKEy2ZCCXCF6-w5H2rrlenUwIRAJQxr9XjjBotcgj5jb6AJ7DA4-JQxP3zg" }

  context "should return last contacts" do
    it { GmailOauth.last_contacts(email, token).should be_kind_of(String) }
  end

end

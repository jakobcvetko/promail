require 'mail'

module GmailOauth

  # Gmail Folders
  #
  # INBOX
  # [Gmail]/All Mail
  # [Gmail]/Drafts
  # [Gmail]/Sent Mail
  # [Gmail]/Spam
  # [Gmail]/Starred
  # [Gmail]/Trash

  def self.last_contacts email, token

    imap = Net::IMAP.new('imap.gmail.com', 993, usessl = true, certs = nil, verify = false)

    messages = []
    begin
      imap.authenticate('XOAUTH2', email, token)
      imap.select('[Gmail]/Sent Mail')

      mail_ids = imap.search(['ALL']).last(10)

      imap.fetch(mail_ids,'RFC822').each do |raw_mail|
        mail = Mail.read_from_string(raw_mail.attr['RFC822'])

        messages.push({
          from: mail.from,
          to: mail.to,
          subject: mail.subject
        })
      end

    rescue Exception => e
      pre = e.to_s
    end
    imap.logout
    imap.disconnect

    # message = messages.join(", ")
    messages
  end
end
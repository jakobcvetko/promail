require 'gmail_oauth'

class Api::ContactsController < ApplicationController

  respond_to :json

  def index

    email = params[:email]
    token = params[:token]

    unless email.present? and token.present?
      render text: "I need email and token!"
      return
    end

    @contacts = GmailOauth.last_contacts(email, token)

    # render text: "OK: #{@last_contacts.to_s}"
  end

end

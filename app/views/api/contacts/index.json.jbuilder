
json.mails(@contacts) do |mail|
  json.from mail[:from].join(', ')
  json.to mail[:to].join(', ')
  json.subject mail[:subject]
end

